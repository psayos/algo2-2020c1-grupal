//
// Created by Pau on 07/07/2020.
//

#ifndef AED2_MAPA_H
#define AED2_MAPA_H

#include <iostream>
#include <list>
#include <set>
#include "Tipos.h"

using namespace std;
using Nat = unsigned int;

class Mapa {
public:
    Mapa();
    Mapa(Mapa const &m);
    bool hayRio(Casilla c) const;
    void agregarRio(Direccion d, Nat p);
    void agregarRios(set<Nat> hs, set<Nat> vs);
    void unirMapa(Mapa m2);
private:
    set<Nat> horizontales_;
    set<Nat> verticales_;

};


class aed2_Mapa {
public:
    aed2_Mapa();
    void agregarRio(Direccion d, Nat p);
    bool hayRio(Casilla c) const;
    void unirMapa(aed2_Mapa m2);
    aed2_Mapa(Mapa m);
    Mapa mapa();

private:
    Mapa mapa_;
};

#endif // AED2_MAPA_H