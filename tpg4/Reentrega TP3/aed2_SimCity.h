//
// Created by Pau on 07/07/2020.
//

#ifndef AED2_SIMCITY_H
#define AED2_SIMCITY_H

#include <iostream>
#include <set>
#include "aed2_Mapa.h"
#include <list>

using namespace std;

using Nat = unsigned int;

class SimCity{
public:
    SimCity(Mapa m);
    void agregarCasa(Casilla c);
    void agregarComercio(Casilla c);
    void avanzarTurno();
    void unir(SimCity sc);

    // Observadores:
    Mapa mapa() const;
    set<Casilla> casas();
    set<Casilla> comercios();
    Nat nivel(Casilla c);
    bool huboConstruccion() const;
    Nat popularidad() const;
    Nat antiguedad() const;


private:
    list<pair<Casilla,Nat>> comercios_;
    list<pair<Casilla,Nat>> casas_;
    bool huboConstruccion_;
    Nat popularidad_;
    Nat turnoActual_;
    aed2_Mapa mapa_;
    list<SimCity> uniones_;

    // Auxiliares

    Nat distancia(Casilla c1, Casilla c2) const;
    Nat nivelMax(Casilla c) const;
    void actualizarNivelComercios();
    void subirNivelComercios();
    void subirNivelCasa();
    void unirACasas();
    void unirAComercios();
};


class aed2_SimCity {
public:
    aed2_SimCity(aed2_Mapa m);
    void agregarCasa(Casilla c);
    void agregarComercio(Casilla c);
    void avanzarTurno();
    void unir(const aed2_SimCity& sc);

    // Observadores:
    Mapa mapa() const;
    set<Casilla> casas();
    set<Casilla> comercios();
    Nat nivel(Casilla c);
    bool huboConstruccion();
    Nat popularidad();
    Nat antiguedad();

    // Conversiones
    SimCity simCity();

private:
    SimCity simcity_;
};

#endif // AED2_SIMCITY_H
