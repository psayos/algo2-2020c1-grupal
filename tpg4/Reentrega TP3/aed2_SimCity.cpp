#include "aed2_SimCity.h"



// Generadores:
SimCity::SimCity(Mapa m) : comercios_(), casas_(), huboConstruccion_(false), popularidad_(0), turnoActual_(0), uniones_(), mapa_(m) {}
void SimCity::agregarCasa(Casilla c) {
    casas_.push_back(make_pair(c, 0)); // O(1) agrega atras una casilla que no estaba en una lista
    huboConstruccion_ = true;
}

void SimCity::agregarComercio(Casilla c) {
    comercios_.push_back(make_pair(c, 0)); // O(1) agrega atras una casilla que no estaba en una lista
    huboConstruccion_ = true;
}

void SimCity::avanzarTurno() {
     subirNivelComercios();
     subirNivelCasa();
    turnoActual_++;
    huboConstruccion_ = false;
}

void SimCity::unir(SimCity sc) {
    uniones_.push_back(sc); // O(copy(SimCity))
    popularidad_ = popularidad_ + sc.popularidad() + 1;
    huboConstruccion_ = huboConstruccion_ || sc.huboConstruccion_;
    if (sc.antiguedad() > turnoActual_) {
        turnoActual_ = sc.antiguedad();
    }
}

// Observadores


Mapa SimCity::mapa() const {
    list<SimCity> uniones = uniones_;
    aed2_Mapa map = mapa_;
    for (SimCity s : uniones) {
        map.unirMapa(s.mapa());
    }
    return map.mapa();
}

set<Casilla> SimCity::casas() {
    unirACasas();
    set<Casilla> casas;
    for (pair<Casilla, Nat> casa : casas_) {
        casas.insert(casa.first);
    }
    return casas;
}

set<Casilla> SimCity::comercios() {
    unirACasas();
    unirAComercios();
    set<Casilla> comercios;
    for (pair<Casilla, Nat> comercio : comercios_) {
        comercios.insert(comercio.first);
    }
    return comercios;
}

Nat SimCity::nivel(Casilla c) {
    unirACasas();
    unirAComercios();
    for (pair<Casilla, Nat> casa : casas_) {
        if (casa.first == c) {
            return casa.second;
        }
    }
    for (pair<Casilla, Nat> comercio : comercios_) {
        if (comercio.first == c) {
            return comercio.second;
        }
    }
}

bool SimCity::huboConstruccion() const {
    return huboConstruccion_;
}

Nat SimCity::popularidad() const {
    return popularidad_;
}

Nat SimCity::antiguedad() const {
    return turnoActual_;
}


// Otras operaciones:
void SimCity::subirNivelComercios(){
    actualizarNivelComercios();
    unirACasas();
    unirAComercios();
    list<pair<Casilla, Nat>> comerciosAux;
    for (pair<Casilla, Nat> comercio : comercios_) {
        comerciosAux.push_back(make_pair(comercio.first, comercio.second + 1));
    }
    comercios_ = comerciosAux;
}

void SimCity::subirNivelCasa() {
    unirACasas();
    list<pair<Casilla, Nat>> casasAux;
    for (pair<Casilla, Nat> casa : casas_) {
        casasAux.push_back(make_pair(casa.first, casa.second + 1));
    }
    casas_ = casasAux;
}

Nat SimCity::distancia(Casilla c1, Casilla c2) const {
    return abs(c1.first - c2.first) + abs(c1.second - c2.second);
}
Nat SimCity::nivelMax(Casilla c) const {
    Nat nivel = 0;
    for (pair<Casilla, Nat> casa : casas_) {
        if (distancia(c, casa.first) <= 3 && casa.second > nivel) {
            nivel = casa.second;
        }
    }
    return nivel;
}

void SimCity::actualizarNivelComercios() {
    list<pair<Casilla, Nat>> comerciosAux;
    for (pair<Casilla, Nat> cm: comercios_) {
        if (cm.second < nivelMax(cm.first)) {
            comerciosAux.push_back(make_pair(cm.first, nivelMax(cm.first)));
        } else {
            comerciosAux.push_back(cm);
        }
    }
    comercios_ = comerciosAux;
}

void SimCity::unirACasas() {
    if (!uniones_.empty()) {
        for (auto s : uniones_) {
            s.unirACasas();
            casas_.splice(casas_.end(), s.casas_);
            }
        }
    list<pair<Casilla, Nat>> casasAux;
    for (pair<Casilla, Nat> casa : casas_) {
        Nat nivel = casa.second;
        for (pair<Casilla, Nat> casa1 : casas_) {
            if (casa.first == casa1.first && casa1.second > casa.second) {
                nivel = casa1.second;
            }
        }
        casasAux.push_back(make_pair(casa.first, nivel));
    }
    casas_ = casasAux;
}

void SimCity::unirAComercios() {
    actualizarNivelComercios();
    if (!uniones_.empty()) {
        for (auto s : uniones_) {
            s.unirAComercios();
            comercios_.splice(comercios_.end(), s.comercios_);
        }
    }
    list<pair<Casilla, Nat>> comerciosAux;
    for (pair<Casilla, Nat> comercio : comercios_) {
        Nat nivel = comercio.second;
        for (pair<Casilla, Nat> cm : comercios_) {
            if (comercio.first == cm.first && cm.second > comercio.second) {
                nivel = cm.second;
            }
        }
        bool esta = false;
        for (pair<Casilla, Nat> casa : casas_) {
            if (casa.first == comercio.first) {
                esta = true;
            }
        }
        if (!esta) {
            comerciosAux.push_back(make_pair(comercio.first, nivel));
        }
    }
    comercios_ = comerciosAux;
}







// AED2_SIMCITY
aed2_SimCity::aed2_SimCity(aed2_Mapa m) : simcity_(m.mapa()) {
}

void aed2_SimCity::agregarCasa(Casilla c) {
    this->simcity_.agregarCasa(c);
}

void aed2_SimCity::agregarComercio(Casilla c) {
    this->simcity_.agregarComercio(c);
}

void aed2_SimCity::avanzarTurno() {
    this->simcity_.avanzarTurno();
}

void aed2_SimCity::unir(const aed2_SimCity &sc) {
    this->simcity_.unir(sc.simcity_);
}

Mapa aed2_SimCity::mapa() const {
    return this->simcity_.mapa();
}

set<Casilla> aed2_SimCity::casas() {
    return this->simcity_.casas();
}

set<Casilla> aed2_SimCity::comercios() {
    return this->simcity_.comercios();
}

Nat aed2_SimCity::nivel(Casilla c) {
    return this->simcity_.nivel(c);
}

Nat aed2_SimCity::popularidad() {
    return this->simcity_.popularidad();
}

Nat aed2_SimCity::antiguedad() {
    return this->simcity_.antiguedad();
}

bool aed2_SimCity::huboConstruccion() {
    return this->simcity_.huboConstruccion();
}

SimCity aed2_SimCity::simCity() {
    return this->simcity_;
}