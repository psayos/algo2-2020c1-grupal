#ifndef AED2_MAPA_H
#define AED2_MAPA_H

#include <iostream>
#include <list>
#include <set>
#include "Tipos.h"

using namespace std;
using Nat = unsigned int;

class Mapa {
public:
    Mapa();

    Mapa(Mapa const &m);

    bool hayRio(Casilla c) const;

    void agregarRio(Direccion d, Nat p);

    void agregarRios(set<Nat> hs, set<Nat> vs);

    void unirMapa(Mapa m2);
private:
    set<Nat> horizontales_;
    set<Nat> verticales_;

};


class aed2_Mapa {
public:

    // Generadores:

    aed2_Mapa();

    void agregarRio(Direccion d, Nat p);

    // Observadores:

    bool hayRio(Casilla c) const;

    // Otras operaciones:

    void unirMapa(aed2_Mapa m2);

    // Conversiones

    // Esta función sirve para convertir del Mapa implementado por ustedes
    // a la clase Mapa de la cátedra
    aed2_Mapa(Mapa m);

    // Esta función sirve para convertir del Mapa de la cátedra (aed2_Mapa)
    // a la clase Mapa definida por ustedes
    Mapa mapa();

private:
    Mapa mapa_;
};

#endif // AED2_MAPA_H