#ifndef AED2_SIMCITY_H
#define AED2_SIMCITY_H

#include <iostream>
#include <set>
#include "aed2_Mapa.h"
#include "map"
#include "vector"

using namespace std;

using Nat = unsigned int;
list<Casilla> casillas(list<pair<Casilla, Nat>>& conjCasillas);


class SimCity {
public:
    // Generadores
    SimCity(Mapa m);
    void agregarCasa(Casilla c);
    void agregarComercio(Casilla c);
    void avanzarTurno();
    void unir(const SimCity& sc);

    // Observadores
    Mapa mapa() const;
    list<pair<Casilla,Nat>> casas() const;
    list<pair<Casilla,Nat>> comercios() const;
    Nat nivel(Casilla c) const;
    bool huboConstruccion() const;
    Nat popularidad() const;
    Nat antiguedad() const;
    set<Casilla> casas2() const;
    set<Casilla> comercios2() const;

private:
    list<pair<Casilla, Nat>> casas_;
    list<pair<Casilla, Nat>> comercios_;
    list<SimCity> uniones_;
    bool huboConstruccion_;
    Nat popularidad_;
    Nat turnos_;
    Mapa mapa_;

    Nat maxNivel(pair<Casilla, Nat> c1) const;
    Nat distancia(Casilla c1, Casilla c2) const;
    list<pair<Casilla,Nat>> filtrar(const list<pair<Casilla,Nat>>& res) const;
};


class aed2_SimCity {
public:
    aed2_SimCity(aed2_Mapa m);
    void agregarCasa(Casilla c);
    void agregarComercio(Casilla c);
    void avanzarTurno();
    void unir(const aed2_SimCity& sc);

    // Observadores:
    Mapa mapa() const;
    set<Casilla> casas() const;
    set<Casilla> comercios() const;
    Nat nivel(Casilla c) const;
    bool huboConstruccion() const;
    Nat popularidad() const;
    Nat antiguedad() const;

    // Conversiones
    SimCity simCity();

private:
    SimCity simcity_;
};


#endif // AED2_SIMCITY_H