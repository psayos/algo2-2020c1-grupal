#include "aed2_SimCity.h"

#include <utility>

// Generadores
SimCity::SimCity(Mapa m)
        : casas_(), comercios_(), uniones_(), huboConstruccion_(false), popularidad_(0), turnos_(0),
          mapa_(m) {}

void SimCity::agregarCasa(Casilla c) {
    (this->casas_).push_back(make_pair(c, 0));
    this->huboConstruccion_ = true;
    this->popularidad();
}

void SimCity::agregarComercio(Casilla c) {
    (this->comercios_).push_back(make_pair(c, 0));
    this->huboConstruccion_ = true;
    this->popularidad();
}

void SimCity::unir(const SimCity &sc) {
    this->uniones_.push_back(sc);
    this->huboConstruccion_ = sc.huboConstruccion_ || this->huboConstruccion_;
    this->popularidad_ = this->popularidad_ + sc.popularidad_ + 1;
    if (sc.turnos_ >= this->turnos_) {
        this->turnos_ = sc.turnos_;
    }
}

void SimCity::avanzarTurno() {
    if (this->huboConstruccion()) {
        this->turnos_ = this->turnos_ + 1;
        for(auto itcasas = casas_.begin(); itcasas != casas_.end(); itcasas++){
            itcasas->second++;
        }
        for(auto itComercios = comercios_.begin(); itComercios != comercios_.end(); itComercios++){
            itComercios->second++;
        }
        for(SimCity s: this->uniones_){
            s.avanzarTurno();
        }
        this->huboConstruccion_ = false;
    }
}

// Observadores
Mapa SimCity::mapa() const{
    list<SimCity> ls = this->uniones_;
    Mapa map = this->mapa_;
    for (const SimCity& s : ls) {
        map.unirMapa(s.mapa());
    }
    return map;
}

list<pair<Casilla, Nat>> SimCity::casas() const {
    list<pair<Casilla, Nat>> res;
    for (pair<Casilla, Nat> c : this->casas_) {
        res.push_back(c);
    }
    list<SimCity> conj = this->uniones_;
    for (const SimCity &s : conj) {
        list<pair<Casilla, Nat>> casas = s.casas();
        for (pair<Casilla, Nat> cs : casas) {
            res.push_back(cs);
        }
    }
    return filtrar(res);
}

list<pair<Casilla, Nat>> SimCity::filtrar(const list<pair<Casilla, Nat>>& res) const {
    list<pair<Casilla,Nat>> filtrado;
    for(pair<Casilla,Nat> c1 : res){
        Nat nivel = c1.second;
        for(pair<Casilla,Nat> c2 : res){
            if(c1.first == c2.first && c2.second >= c1.second){
                nivel = c2.second;
            }
        }
        pair<Casilla,Nat> cf = make_pair(c1.first,nivel);
        filtrado.push_back(cf);
    }
    return filtrado;
}


list<pair<Casilla, Nat>> SimCity::comercios() const {
    list<pair<Casilla, Nat>> res;
    for (pair<Casilla, Nat> c : this->comercios_) {
        Nat nivel = maxNivel(c);
        pair<Casilla,Nat> comercio = make_pair(c.first, nivel);
        res.push_back(comercio);
    }
    list<SimCity> conj = this->uniones_;
    for (const SimCity &s : conj) {
        list<pair<Casilla, Nat>> comercios = s.comercios();
        for (pair<Casilla, Nat> cs : comercios) {
            res.push_back(cs);
        }
    }
    return filtrar(res);
}



Nat SimCity::nivel(Casilla c) const {
    list<pair<Casilla, Nat>> casas = this->casas();
    for (pair<Casilla, Nat> pc : casas) {
        if (pc.first == c) {
            return pc.second;
        }
    }
    list<pair<Casilla, Nat>> comercios = this->comercios();
    for (pair<Casilla, Nat> pc2 : comercios) {
        if (pc2.first == c) {
            return pc2.second;
        }
    }
}

Nat SimCity::antiguedad() const {
    return this->turnos_;
}

Nat SimCity::popularidad() const {
    return this->popularidad_;
}

bool SimCity::huboConstruccion() const {
    return this->huboConstruccion_;
}

set<Casilla> SimCity::casas2() const {
    set<Casilla> res;
    list<pair<Casilla,Nat>> casas = this->casas();
    for(pair<Casilla,Nat> pc : casas){
        res.insert(pc.first);
    }
    return res;
}

set<Casilla> SimCity::comercios2() const {
    set<Casilla> res;
    list<pair<Casilla,Nat>> comercios = this->comercios();
    for(pair<Casilla,Nat> pc : comercios){
        res.insert(pc.first);
    }
    return res;
}

// Auxiliares


Nat SimCity::maxNivel(pair<Casilla, Nat> c1) const {
    list<pair<Casilla, Nat>> casas = this->casas();
    Nat nivel = c1.second;
    for (pair<Casilla, Nat> c2 : casas) {
        if (distancia(c1.first, c2.first) <= 3 && nivel <= c2.second) {
            nivel = c2.second;
        }
    }
    return nivel;
}


Nat SimCity::distancia(Casilla c1, Casilla c2) const {
    return abs(c1.first - c2.first) + abs(c1.second - c2.second);
}





// CLASE AED2_SIMCITY

aed2_SimCity::aed2_SimCity(aed2_Mapa m) : simcity_(m.mapa()) {
}

void aed2_SimCity::agregarCasa(Casilla c) {
    this->simcity_.agregarCasa(c);
}

void aed2_SimCity::agregarComercio(Casilla c) {
    this->simcity_.agregarComercio(c);
}

void aed2_SimCity::avanzarTurno() {
    this->simcity_.avanzarTurno();
}

void aed2_SimCity::unir(const aed2_SimCity& sc) {
    this->simcity_.unir(sc.simcity_);
}

Mapa aed2_SimCity::mapa() const {
    return this->simcity_.mapa();
}

set<Casilla> aed2_SimCity::casas() const {
    return this->simcity_.casas2();
}

set<Casilla> aed2_SimCity::comercios() const {
    return this->simcity_.comercios2();
}

Nat aed2_SimCity::nivel(Casilla c) const {
    return this->simcity_.nivel(c);
}

Nat aed2_SimCity::popularidad() const {
    return this->simcity_.popularidad();
}

Nat aed2_SimCity::antiguedad() const {
    return this->simcity_.antiguedad();
}

bool aed2_SimCity::huboConstruccion() const {
    return this->simcity_.huboConstruccion();
}

SimCity aed2_SimCity::simCity() {
    return this->simcity_;
}

