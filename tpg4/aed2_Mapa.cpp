#include "aed2_Mapa.h"

Mapa::Mapa() : verticales_(), horizontales_() {}

Mapa::Mapa(Mapa const &m) : verticales_(m.verticales_), horizontales_(m.horizontales_){}

bool Mapa::hayRio(Casilla c) const {
    set<Nat> riosH = this->horizontales_;
    set<Nat> riosV = this->verticales_;
    for(Nat r : riosH){
        if(r == c.second){
            return true;
        }
    }
    for(Nat r : riosV){
        if(r == c.first){
            return true;
        }
    }
    return false;
}



void Mapa::agregarRio(Direccion d, Nat p) {
    if (d == Horizontal) {
        this->horizontales_.insert(p);
    } else {
        this->verticales_.insert(p);
    }
}

void Mapa::agregarRios(set<Nat> hs, set<Nat> vs) {
    for (Nat r : hs) {
        agregarRio(Horizontal, r);
    }
    for (Nat r : vs) {
        agregarRio(Vertical, r);
    }
}

void Mapa::unirMapa(Mapa m2) {
    agregarRios(m2.horizontales_,m2.verticales_);
}

aed2_Mapa::aed2_Mapa() : mapa_(mapa()) {
}

bool aed2_Mapa::hayRio(Casilla c) const {
    return mapa_.hayRio(c);
}

void aed2_Mapa::agregarRio(Direccion d, Nat p) {
    mapa_.agregarRio(d, p);
}

void aed2_Mapa::unirMapa(aed2_Mapa m2) {
    mapa_.unirMapa(m2.mapa());
}



Mapa aed2_Mapa::mapa() {
    return mapa_;
}

aed2_Mapa::aed2_Mapa(Mapa m) : mapa_(m) {
}
